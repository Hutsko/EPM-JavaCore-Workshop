WHAT YOU CAN FIND IN THIS REPOSITORY.
-----------------------------------------------------

Java Core Workbooks: https://drive.google.com/open?id=1t8EHSygv7C2-1ei_Tw9OcSDxUF3Pu5EI

===========================================================

Create and start an application through a console

Achieved aims:
    1. Compiled the main class and all main related classes.
    2. The application started from console.
============================================================
Custom Class Loader. There are some different ways to load our classes.
Some examples about Exceptions.

Achieved aims:
    1. Loading the main and main-related classes with WildClassLoaderNew. 

Not resolved problems:
    1. main() invocation method don't work.
    2. Loading of classes from URL.
    3. WildClassLoaderOld don't works properly.
============================================================
JMM-errors

Achieved aims: 
    1. Got OutOfMemoryError, StackOverFlowError in different ways and 
    mainly without tuning memory size at JVM start. 

============================================================
Exceptions 

Achieved aims: 
    1. Played with different exceptions\errors

============================================================
Generics

============================================================
String pool

============================================================
Arrays in java

============================================================
Lambdas

============================================================

============================================================

Look 'Description.txt' file and javaDoc for details.