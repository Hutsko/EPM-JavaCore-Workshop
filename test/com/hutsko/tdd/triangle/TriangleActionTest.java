package com.hutsko.tdd.triangle;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TriangleActionTest {

    TriangleAction triangleAction;

    @BeforeEach
    void setUp() {
        triangleAction = new TriangleAction();
    }

    @AfterEach
    void tearDown() {
        triangleAction = null;
    }

    @Test
    public void testTriangleIsEquilateralTrue() {
        triangleAction.setTriangle(3,3,5);
        int actual = triangleAction.getTriangleTypeIndex();
        triangleAction.setTriangle(2,4,4);
        int actual2 = triangleAction.getTriangleTypeIndex();
        triangleAction.setTriangle(5,3,5);
        int actual3 = triangleAction.getTriangleTypeIndex();

        assertEquals(1, actual);
        assertEquals(1, actual2);
        assertEquals(1, actual3);
    }

    @Test
    public void testTriangleIsEquilateralFalse() {
        triangleAction.setTriangle(1,2,3);
        int actual = triangleAction.getTriangleTypeIndex();
        triangleAction.setTriangle(5,6,8);
        int actual2 = triangleAction.getTriangleTypeIndex();
        triangleAction.setTriangle(7,22,15);
        int actual3 = triangleAction.getTriangleTypeIndex();

        assertNotEquals(1, actual);
        assertNotEquals(1, actual2);
        assertNotEquals(1, actual3);
//        Assertions.fail("Test must be used after true-version will succeed");
    }

    @Test
    public void testTriangleIsIsoscelesTrue() {
        triangleAction.setTriangle(4,4,4);
        int actual = triangleAction.getTriangleTypeIndex();
        assertEquals(2, actual);
    }

    @Test
    public void testTriangleIsIsoscelesFalse() {
        triangleAction.setTriangle(4,1,4);
        int actual = triangleAction.getTriangleTypeIndex();
        triangleAction.setTriangle(5,2,3);
        int actual2 = triangleAction.getTriangleTypeIndex();
        triangleAction.setTriangle(6,7,8);
        int actual3 = triangleAction.getTriangleTypeIndex();

        assertNotEquals(2, actual);
        assertNotEquals(2, actual2);
        assertNotEquals(2, actual3);
//        Assertions.fail("Test must be used after true-version will succeed");
    }

    @Test
    public void testTriangleIsVersatileTrue() {
        triangleAction.setTriangle(3,4,5);
        int actual = triangleAction.getTriangleTypeIndex();
        triangleAction.setTriangle(7,12,10);
        int actual2 = triangleAction.getTriangleTypeIndex();
        triangleAction.setTriangle(8,1,7);
        int actual3 = triangleAction.getTriangleTypeIndex();
        assertEquals(3, actual);
        assertEquals(3, actual2);
        assertEquals(3, actual3);
    }

    @Test
    public void testTriangleIsVersatileFalse() {
        triangleAction.setTriangle(3,3,5);
        int actual = triangleAction.getTriangleTypeIndex();
        triangleAction.setTriangle(4,4,4);
        int actual2 = triangleAction.getTriangleTypeIndex();
        assertNotEquals(3, actual);
        assertNotEquals(3, actual2);
//        Assertions.fail("Test must be used after true-version will succeed");

    }

    @Test
    public void testTriangleBuildImpossible() {
        triangleAction.setTriangle(0,0,0);
        try {
            triangleAction.getTriangleTypeIndex();
            Assertions.fail("The exception wasn't thrown");
        } catch (TriangleException e) {
            assertEquals(e.getClass(), TriangleException.class);
        }

    }
    @Test
    public void testTriangleBuildImpossible2() {
        triangleAction.setTriangle(10,4,5);
        try {
            triangleAction.getTriangleTypeIndex();
            Assertions.fail("The exception wasn't thrown");
        } catch (TriangleException e) {
            assertEquals(e.getClass(), TriangleException.class);
        }

    }
    @Test
    public void testTriangleBuildImpossible3() {
        triangleAction.setTriangle(0,4,5);
        try {
            triangleAction.getTriangleTypeIndex();
            Assertions.fail("The exception wasn't thrown");
        } catch (TriangleException e) {
            assertEquals(e.getClass(), TriangleException.class);
        }

    }
}