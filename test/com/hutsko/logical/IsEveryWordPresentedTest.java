package com.hutsko.logical;

import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

class IsEveryWordPresentedTest {
    private static IsEveryWordPresented testedClass;
    private static List<String> magazine;
    private static List<String> letter;
    private static Map<String, Integer> magazineMap;
    private static Map<String, Integer> letterMap;

    @BeforeEach
    void setUp() {
        testedClass = new IsEveryWordPresented();
        magazine = new ArrayList<>();
        letter = new ArrayList<>();
    }
    @AfterEach
    void tearDown(){
        testedClass = null;
        magazine = null;
        letter = null;
        magazineMap = null;
        letterMap = null;
    }

    @Test
    // check that all passed values are saved to Map
    public void convertToMapTest(){
        magazine.add("Masons");
        magazine.add("have");
        magazine.add("money");
        magazine.add("money");

        magazineMap = testedClass.convertToMap(magazine);
        Assertions.assertEquals(3, magazineMap.size());
        Assertions.assertEquals((int) magazineMap.get("money"),2);
    }

    @Test
    public void isThreatPossiblePositiveTest1(){
        magazine.add("1");
        magazine.add("2");
        magazine.add("3");
        magazineMap = testedClass.convertToMap(magazine);
        letter.add("1");
        letter.add("2");
        letterMap = testedClass.convertToMap(letter);

        Assertions.assertTrue(testedClass.isThreatPossible(magazineMap, letterMap));
    }

    @Test
    public void isThreatPossiblePositiveTest2(){
        magazine.add("1");
        magazine.add("2");
        magazineMap = testedClass.convertToMap(magazine);
        letter.add("1");
        letter.add("2");
        letterMap = testedClass.convertToMap(letter);

        Assertions.assertTrue(testedClass.isThreatPossible(magazineMap, letterMap));
    }
    @Test
    public void isThreatPossiblePositiveTest3(){
        magazine.add("1");
        magazine.add("1");
        magazine.add("2");
        magazine.add("2");
        magazineMap = testedClass.convertToMap(magazine);
        letter.add("1");
        letter.add("2");
        letter.add("2");
        letterMap = testedClass.convertToMap(letter);

        Assertions.assertTrue(testedClass.isThreatPossible(magazineMap, letterMap));
    }

    @Test
    public void isThreatPossibleNegativeTest1(){
        magazine.add("1");
        magazine.add("2");
        magazineMap = testedClass.convertToMap(magazine);
        letter.add("2");
        letter.add("2");
        letterMap = testedClass.convertToMap(letter);

        Assertions.assertFalse(testedClass.isThreatPossible(magazineMap, letterMap));
    }
    @Test
    public void isThreatPossibleNegativeTest2(){
        magazine.add("3");
        magazine.add("4");
        magazineMap = testedClass.convertToMap(magazine);
        letter.add("4");
        letter.add("5");
        letterMap = testedClass.convertToMap(letter);

        Assertions.assertFalse(testedClass.isThreatPossible(magazineMap, letterMap));
    }
    @Test
    public void isThreatPossibleNegativeTest3(){
        magazine.add("a");
        magazine.add("A");
        magazineMap = testedClass.convertToMap(magazine);
        letter.add("a");
        letter.add("a");
        letterMap = testedClass.convertToMap(letter);

        Assertions.assertFalse(testedClass.isThreatPossible(magazineMap, letterMap));
    }

    @Test
    public void isThreatPossibleNegativeTest4(){
        magazine.add("B");
        magazine.add("B");
        magazineMap = testedClass.convertToMap(magazine);
        letter.add("B");
        letter.add("b");
        letterMap = testedClass.convertToMap(letter);

        Assertions.assertFalse(testedClass.isThreatPossible(magazineMap, letterMap));
    }
    @Test
    public void isThreatPossibleEmptyMapTest1(){
        magazineMap = testedClass.convertToMap(magazine);
        letterMap = testedClass.convertToMap(letter);

        Assertions.assertTrue(testedClass.isThreatPossible(magazineMap, letterMap));
    }
    @Test
    public void isThreatPossibleEmptyMapTest2(){
        magazine.add("1");
        magazineMap = testedClass.convertToMap(magazine);
        letterMap = testedClass.convertToMap(letter);

        Assertions.assertTrue(testedClass.isThreatPossible(magazineMap, letterMap));
    }
    @Test
    public void isThreatPossibleEmptyMapTest3(){
        magazineMap = testedClass.convertToMap(magazine);
        letter.add("1");
        letterMap = testedClass.convertToMap(letter);
        Assertions.assertFalse(testedClass.isThreatPossible(magazineMap, letterMap));
    }
}