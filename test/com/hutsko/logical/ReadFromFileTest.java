package com.hutsko.logical;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

public class ReadFromFileTest {

    private static final String DIRECTORY = "test/resources/";
    private static IsEveryWordPresented testedClass;
    private static List<String> text;

    @BeforeAll
    static void setUp() {
        testedClass = new IsEveryWordPresented();
    }

    @AfterAll
    static void tearDown() {

    }

    @Test
    public void readTextFromFileTest() {
//        text = testedClass.readTextFromFile(DIRECTORY, "letter.txt");
        text = testedClass.readTextFromFile(DIRECTORY, "magazine.txt");
        System.out.println(text);
        System.out.println(text.size());
        Assertions.assertEquals(9, text.size());
    }

    @Test
    public void searchTextFromFileTest() {
        List<String> book = testedClass.readTextFromFile(DIRECTORY, "magazine.txt");
        List<String> letter = testedClass.readTextFromFile(DIRECTORY, "letter.txt");

        Map<String, Integer> bookMap = testedClass.convertToMap(book);
        Map<String, Integer> letterMap = testedClass.convertToMap(letter);

        boolean answer = testedClass.isThreatPossible(bookMap, letterMap);

        Assertions.assertTrue(answer);
    }
}
