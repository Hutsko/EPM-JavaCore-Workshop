package com.hutsko.task3.lambdas;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * <p>This class demonstrate how to handle with collection via lambdas.</p>
 * <p>I Used two different notations for the same results</p>
 * <code>Person</code> declared as inner class.
 */
public class ComparatorLambda {
    private static List<Person> personList = new ArrayList<>();

    public static void main(String[] args) {
        Person p = new Person("Mike", 21);
        personList.add(p);
        p = new Person("Jane", 24);
        personList.add(p);
        p = new Person("Eugene", 46);
        personList.add(p);
        p = new Person("Michelangelo", 34);
        personList.add(p);
        p = new Person("Ivan", 87);
        personList.add(p);
        p = new Person("Nicola", 34);
        personList.add(p);
        p = new Person("Susan", 29);
        personList.add(p);

        personList.sort((o1, o2) -> o1.getName().compareTo(o2.getName()));  //lambda for comparator
        personList.forEach(person -> System.out.println(person));           // lambda for printing

        System.out.println();

        personList.sort(Comparator.comparingInt(Person::getAge));       //another notation for lambda
        personList.forEach(System.out::println);                        //another notation for the same printing
    }
}

class Person {
    private String name;
    private int age;

    Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
