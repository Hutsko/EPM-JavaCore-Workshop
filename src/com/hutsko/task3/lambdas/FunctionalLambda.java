package com.hutsko.task3.lambdas;

import java.util.function.Function;

/**
 * There I implement Function interface and use lambda for simple multiplication of two digits.
 */
public class FunctionalLambda {
    public static Function<Integer, Integer> doMultiplication(int a) {
        int n = 0;
        return arg -> {
            System.out.print(a + "*" + arg + " = ");
            arg *= a;
//            n *= a; // variable must be final of effective final
            return n + arg;
        };
    }

    public static void main(String[] args) {
        int z = doMultiplication(3).apply(7);
        System.out.println(z);
    }
}
