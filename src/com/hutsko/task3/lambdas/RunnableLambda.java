package com.hutsko.task3.lambdas;


/**
 * This class demonstrate different ways of how to handle with lambdas and pass them into distinct thread
 */
public class RunnableLambda {

    public static void main(String[] args) {

        OneRunnable t1 = new OneRunnable(() -> System.out.println(
                "Sometimes " + "\\" + Thread.currentThread().getName())); //passed lambda into thread-extended class

        Runnable t2 = new Runnable() {                                    //create new Runnable via anonymous class.
            @Override
            public void run() {
                System.out.println("I speak " + "\\" + Thread.currentThread().getName());
            } };

        Runnable t3 = () -> { System.out.println("like Master Yoda " + "\\" + Thread.currentThread().getName());}; //create new Runnable object with lambda and passed it into new Thread.
        t1.start();
        new Thread(t2).start();
        new Thread(t3).start();

    }

    private static void makeRunable(Runnable r) {
        r.run();
        ;
    }

    private static class OneRunnable extends Thread {
        Runnable runnable;

        public OneRunnable(Runnable runnable) {
            this.runnable = runnable;
        }

        public void run() {
            runnable.run();
        }
    }
}
