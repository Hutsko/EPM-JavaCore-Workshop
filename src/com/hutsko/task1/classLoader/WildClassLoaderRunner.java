package com.hutsko.task1.classLoader;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class WildClassLoaderRunner {

    public static void main(String[] args) {
        WildClassLoaderNew wildClassLoader = new WildClassLoaderNew(null);
        try {
            Class clazz = wildClassLoader.findClass("com.hutsko.classLoader.entity.Main");
            System.out.println(clazz.getClass() + " loaded with " + clazz.getClassLoader().toString());
            Method main = (Method) clazz.getMethod("startActions").invoke(null);

        } catch (ClassNotFoundException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }

    }
}