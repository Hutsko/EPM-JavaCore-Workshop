package com.hutsko.task1.classLoader;

import java.io.FileInputStream;
import java.io.IOException;

/**
 * Custom Class Loader. It demonstrate how to load our classes with one ClassLoader
 * This classLoader use new approach implemented in java1.2 by overriding findClass() method.</p>
 *
 * <p>Set proper path in BIN_PATH for your enviroment.
 * @Source https://habr.com/post/104229/
 */
public class WildClassLoaderNew extends ClassLoader {
    private final static String BIN_PATH = "C:/Users/Yauhen_Hutsko/IdeaProjects/customClassLoader/out/production/customClassLoader/";

    //set parent = null for loading dependent classes with our WCL
    public WildClassLoaderNew(ClassLoader parent) {
        super(parent);
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        FileInputStream fis = null;
        System.out.println("Attempting to load " + name + " with classloader: " + this.getClass().getName());
        String prepName = name.replace('.', '/');
        try {
            String fullPath = BIN_PATH + prepName + ".class";
            fis = new FileInputStream(fullPath);
            byte[] classBytes = new byte[fis.available()];
            fis.read(classBytes);
            System.out.println("Class " + name + " loaded.");
            return defineClass(name, classBytes, 0, classBytes.length);
        } catch (IOException e) {
            throw new ClassNotFoundException(name);
        } finally {
            if (null != fis) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
