package com.hutsko.task1.classLoader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Disabled because don't work.
 * This classLoader use old java1.1 approach by overriding loadClass() method.
 * @Source https://www.ibm.com/developerworks/java/tutorials/j-classloader/j-classloader.html#
 */
public class WildClassLoaderOld extends ClassLoader {

    private byte[] getBytes(String filename) throws IOException {
//        URI remoteClass = URI.create(filename);

        File file = new File(filename);
        long length = file.length();

        byte[] raw = new byte[(int) length];
        FileInputStream byteStream = new FileInputStream(file);
        int bytes = byteStream.read(raw);
        if (bytes != length) {
            throw new IOException("Can't read all bytes");
        }
        byteStream.close();
        return raw;
    }

    private boolean compile(String javaFile) throws IOException {
        System.out.println("WCL: Compiling " + javaFile);
         Process process = Runtime.getRuntime().exec("javac " + javaFile);
        try {
            process.waitFor();
        } catch (InterruptedException e) {
            System.out.println(e);
        }
        int returnCode = process.exitValue();
        return returnCode == 0;
    }

    @Override
    protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
        Class clas = null;
        clas = findLoadedClass(name);

        String fileStub = name.replace('.', '/');
        String javaFileName = fileStub + ".java";
        String classFileName = fileStub + ".class";
        File javaFile = new File(javaFileName);
        File classFile = new File(classFileName);

        if (javaFile.exists() &&
                (!classFile.exists() || javaFile.lastModified() > classFile.lastModified())) {
            try {
                if (!compile(javaFileName) || !classFile.exists()) {
                    throw new ClassNotFoundException("Compile failed: " + javaFileName);
                }
            } catch (IOException e) {
                throw new ClassNotFoundException(e.toString());
            }
        }
        try {
            byte[] raw = getBytes(classFileName);
            clas = defineClass(name, raw, 0, raw.length);
        } catch (IOException e) {
            //todo: this getTriangleTypeIndex not fail.
        }
        if (clas == null) {
            clas = findSystemClass(name);
        }
        if (resolve && clas != null) {
            resolveClass(clas);
        }
        if (clas == null) {
            throw new ClassNotFoundException(name);
        }
        return clas;
    }
}
