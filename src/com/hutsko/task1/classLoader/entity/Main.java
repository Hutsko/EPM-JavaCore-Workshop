package com.hutsko.task1.classLoader.entity;

public class Main {

    public static void main(String[] args) {
        startActions();
    }

    public static void startActions(){
        Animal cat = new Cat();
        Animal dog = new Dog();
        cat.play();
        dog.play();
        cat.voice();
        dog.voice();
        System.out.println("Cat.class loaded with " + cat.getClass().getClassLoader().toString());
        System.out.println("Dog.class loaded with " + dog.getClass().getClassLoader().toString());
    }

}
