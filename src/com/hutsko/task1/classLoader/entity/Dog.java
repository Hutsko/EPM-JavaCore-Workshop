package com.hutsko.task1.classLoader.entity;

public class Dog implements Animal{

    @Override
    public void play() {
        System.out.println("Dog plays");
    }

    @Override
    public void voice() {
        System.out.println("Dog barks");
    }
}
