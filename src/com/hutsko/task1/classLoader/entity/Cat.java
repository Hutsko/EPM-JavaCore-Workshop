package com.hutsko.task1.classLoader.entity;

public class Cat implements Animal {
    @Override
    public void play() {
        System.out.println("Cat plays");
    }

    @Override
    public void voice() {
        System.out.println("Cat meows");
    }
}
