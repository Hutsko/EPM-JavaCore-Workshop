package com.hutsko.task1.classLoader.entity;

public interface Animal {
    void play();
    void voice();

}
