package com.hutsko.array;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.Arrays;

/**
 *
 * We can't manage to massive's objects as usual.
 * Nevertheless they reveal all qualities of objects.
 * @see java.lang.reflect.Array
 */
public class Main {
    public static void main(String[] args) {
        Integer[] array = {1,2,3};
        int a = array.length;
        Arrays.stream(array).forEach(value -> System.out.println(value));
        String strings[] = (String[]) Array.newInstance(String.class, 5);
        System.out.println(strings instanceof String[]);    //true


        System.out.println(array instanceof Object);        //true
        System.out.println(array instanceof Serializable);  //true
        System.out.println(array instanceof Cloneable);     //true
//        System.out.println(array instanceof Iterable); //массив не реализует Collection

    }
}
