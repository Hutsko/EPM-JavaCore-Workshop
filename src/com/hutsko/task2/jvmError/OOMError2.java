package com.hutsko.task2.jvmError;

import com.hutsko.task1.classLoader.WildClassLoaderNew;
import com.hutsko.task1.classLoader.WildClassLoaderOld;
import com.hutsko.task1.classLoader.WildClassLoaderRunner;

import java.beans.beancontext.BeanContext;
import java.beans.beancontext.BeanContextServicesSupport;
import java.time.LocalDateTime;

/**
 * <p> The aim: java.lang.OutOfMemoryError: Java heap space without using collections or arrays </p>
 * <p> Also may produce java.lang.OutOfMemoryError: GC overhead limit exceeded
 * This code creates tons of new <code>DataHolder</code> objects each in distinct thread. Threads still alive
 * long enough to Main method ends his work. Every object contain bunch of insignificant objects with purpose to get
 * the heavier objects. </p>
 * <p> In this scenario, the application stops with error near 57000th thread. And this getTriangleTypeIndex a quite long process </p>
 * <p> The easies way getTriangleTypeIndex use -Xmx:{size} for setting max heap-memory size.</p>
 */
public class OOMError2 {
    volatile static String name = "x";

    public static void main(String[] args) {
        for (int i = 0; i < 120_000; i++) {
            name += "x";
            DataHolder holder = new DataHolder();
            new Thread(holder).start();
        }
        System.out.println("Main method done his work");
    }

    static class DataHolder implements Runnable {
        Long currTime = System.currentTimeMillis();
        Double aDouble = 2.0;
        String n = new String(name);
        Integer i = new Integer(1);
        LocalDateTime ldt = LocalDateTime.now();
        Boolean b = new Boolean(false);
        ClassLoader loader = new WildClassLoaderNew(null);
        ClassLoader loader2 = new WildClassLoaderOld();
        BeanContext context = new BeanContextServicesSupport();
        WildClassLoaderRunner runner = new WildClassLoaderRunner();

        @Override
        public void run() {
            System.out.println(Thread.currentThread() + " was started.");
            try {
                Class clazz = loader2.loadClass("com.hutsko.task1.classLoader.entity.Main");
            } catch (ClassNotFoundException e) {
                System.out.println("class not found");
            }
            try {
                Thread.sleep(240_000); //4 min delay before thread ends.
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread() + " ended");
        }
    }
}
