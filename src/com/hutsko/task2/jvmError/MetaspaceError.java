package com.hutsko.task2.jvmError;


import com.hutsko.task1.classLoader.WildClassLoaderNew;
import com.hutsko.task1.classLoader.WildClassLoaderOld;

import java.util.ArrayList;
import java.util.List;

/**
 * <p> The aim: java.lang.OutOfMemoryError: Metaspace. Load classes continuously and make them stay in memory. </p>
 * <p> Shortest way getTriangleTypeIndex to set Metaspace size via "-XX:MaxMetaspaceSize=100m" command</p>
 *
 * <p> Without restriction memory size  it throws java.lang.OutOfMemoryError: Compressed class space</p>
 */
public class MetaspaceError {
    static List<Class> classes = new ArrayList<>();
    static List<ClassLoader> loaders = new ArrayList<>();

    public static void main(String[] args) {

        for (int i = 0; i < 100_000_000; i++) {
            loaders.add(new WildClassLoaderNew(null));
            try {
                classes.add(loaders.get(i).loadClass("com.hutsko.task1.classLoader.entity.Main"));
                System.out.println(i);
            } catch (ClassNotFoundException e) {
                System.out.println("not so easy!");
            }
        }
        classes.forEach(aClass -> System.out.println(classes.indexOf(aClass)));
    }


}
