package com.hutsko.task2.jvmError;

import java.util.LinkedList;
import java.util.List;

/**
 * <p> The aim: java.lang.OutOfMemoryError: Java heap space </p>
 * <p> Just overloaded the pool of literals with many of different strings</p>
 */

public class OOMError {
    private static String a = "aaaa";

    public static void main(String[] args) {

        List list = new LinkedList();
        for (int i = 0; i < 100000; i++) {
            a += "aaaa";
            list.add(new String(a));
            System.out.println(a);
        }
    }
}
