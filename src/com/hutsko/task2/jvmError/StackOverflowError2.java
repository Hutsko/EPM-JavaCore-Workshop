package com.hutsko.task2.jvmError;

import java.util.LinkedList;
import java.util.List;

/**
 * <p>The aim: java.lang.StackOverflowError. Do not use recursive methods. Don’t tune stack size.</p>
 * <p>It getTriangleTypeIndex almost the same as recursive invoking of method but in this particular case I used
 * varargs-method counterUp(Object...row) which invokes every time on new object with different parameters.
 * This code perform about 10k iterations before StackOverFlowError and takes a little time.</p>
 */
public class StackOverflowError2 {
    private static List<LittleObject> objects = new LinkedList<>();
    private static int counter = 0;

    public static void main(String[] args) {
        System.out.println("Creating of new objects. It takes about 1 min...\n" +
                "Then, next couple of minutes you will see plenty changing symbols. \nAnd finally - StackOverFlowError");

        for (int i = 0; i < 15000; i++) {
            objects.add(new LittleObject(i));
        }
        objects.get(counter).counterUp(objects.get(counter).localParams.toArray());
    }

    static class LittleObject {
        static List<Integer> params = new LinkedList<>();
        List<Integer> localParams = new LinkedList<>();

        LittleObject(int simpleData) {
            simpleData += 1;
            params.add(simpleData);
            localParams.addAll(params);
        }

        void counterUp(Object... l) {
            StackOverflowError2.counter++;
            for (Object inf : l) {
                System.out.print(inf + " ");
            }
            System.out.println();
            while (StackOverflowError2.counter < StackOverflowError2.objects.size()) {
                counterUp(StackOverflowError2.objects.get(StackOverflowError2.counter).localParams.toArray());
            }
        }
    }

}


