package com.hutsko.task2.jvmError;

/**
 * <p> The aim: <code>java.lang.StackOverflowError.</code> Use recursive methods. Don’t tune stack size. </p>
 * <p>
 * Simple recursive call of method <code>a()</code>. Or mutual invocation of methods <code>b()</code> and <code>c()</code> produce StackOverFlowError.
 * Use comments for switching between these variants.
 * <p>
 * Notion: Stack-memory has limited size and  can accommodate a different quantity of methods. Depends on their weight.
 * Just comment all primitives in <code>a()</code> method to see the difference.
 */
public class StackOverflowError {

    public static void main(String[] args) {
        StackOverflowError o = new StackOverflowError();
        // we may invoke method recursively
        o.a(1);
        // or via mutual invoking
//        b();
    }

    private void a(long i) {
        int z = 100000;
        int x = 20000;
        int v = 34444;
        int fv = 411111;
        int vsdga = 52222;
        int gdfv = 445454;
        int vhdf = 1212127;

        System.out.println(i);
        i = i + 1;
        a(i);
    }
//    private static void b(){
//        c();
//    }
//    private static void c(){
//        b();
//    }
}
