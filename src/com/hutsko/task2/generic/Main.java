package com.hutsko.task2.generic;

/**
 *
 */
public class Main {
        public static void main(String[ ] args) {
            // параметризация типом Integer
            Message<Integer> ob1 = new Message<Integer>();
            ob1.setValue(1); // возможен только тип Integer для метода setValue
            System.out.println(ob1.getValue());
            // параметризация типом String
            Message<String> ob2 = new Message<String>("Java");
            System.out.println(ob2.getValue());
            // ob1 = ob2; // ошибка компиляции – параметризация нековариантна
            // параметризация по умолчанию – Object
            Message ob3 = new Message(); // warning – raw type
            ob3 = ob1; // нет ошибки компиляции – тип ссылки ob3 - Object. Она вмещает любой тип.
            System.out.println(ob3.getValue());
            ob3.setValue(new Byte((byte)1));
            System.out.println(ob3); /* выводится тип объекта, а не тип параметризации */
            ob3.setValue("71");
            System.out.println(ob3); /* выводится тип объекта, а не тип параметризации */
            ob3.setValue(71);
            System.out.println(ob3); /* выводится тип объекта, а не тип параметризации */
            ob3.setValue(null);
        }
    }

    class Message <T> {
        private T value;
        public Message() {
        }
        public Message (T value) {
            this.value = value;
        }
        public T getValue() {
            return value;
        }
        public void setValue(T value) {
            this.value = value;
        }
        public String toString() {
            if (value == null) {
                return null;
            }
            return value.getClass().getName() + " :" + value;
        }
    }
