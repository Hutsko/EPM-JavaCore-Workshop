package com.hutsko.task2.stringPool;

/**
 * Operate with string pool via equals, ==, intern().
 */
public class Main {
    static String a = "a";
    static String b = "b";
    static String c = a + b;
    static String cc = "a" + "b";
    static String d = "ab";

    public static void main(String[] args) {

        System.out.println("(c == d) = " + (c == d));               //false
        System.out.println("c.equals(d) = " + c.equals(d));         //true
        System.out.println("(cc == d) = " + (cc == d));             //true !!
        System.out.println("cc.equals(d) = " + cc.equals(d));       //true
        System.out.println("(cc == c) = " + (cc == c));             //false
        System.out.println("(cc.equals(c)) = " + (cc.equals(c)) );  //true

        System.out.println(c.intern() == d.intern());
        System.out.println(c.intern() == cc.intern());
    }
}
