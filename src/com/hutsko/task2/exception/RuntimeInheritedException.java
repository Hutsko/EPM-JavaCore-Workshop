package com.hutsko.task2.exception;

public class RuntimeInheritedException extends RuntimeException {
    public RuntimeInheritedException(String message) {
        super(message);
    }

    public RuntimeInheritedException(String message, Throwable cause) {
        super(message, cause);
    }

    public RuntimeInheritedException() {
    }
}
