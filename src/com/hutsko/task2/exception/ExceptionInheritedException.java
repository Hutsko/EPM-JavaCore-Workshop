package com.hutsko.task2.exception;

public class ExceptionInheritedException extends Exception {
    public ExceptionInheritedException() {
    }

    public ExceptionInheritedException(String message) {
        super(message);
    }

    public ExceptionInheritedException(String message, Throwable cause) {
        super(message, cause);
    }

    public ExceptionInheritedException(Throwable cause) {
        super(cause);
    }
}
