package com.hutsko.task2.exception;

public class ErrorInheritedException extends Error{
    public ErrorInheritedException() {
    }

    public ErrorInheritedException(String message) {
        super(message);
    }

    public ErrorInheritedException(String message, Throwable cause) {
        super(message, cause);
    }

    public ErrorInheritedException(Throwable cause) {
        super(cause);
    }
}
