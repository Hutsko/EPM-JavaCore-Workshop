package com.hutsko.task2.exception;

/**
 * There used three different types of exceptions(error, runtimeException, exception) as a parent for mines.
 */
public class Main {
    public static void main(String[] args) {
        try {
            amazingMethod();
        } catch (ErrorInheritedException e) {
            System.out.println(e.getClass().getSimpleName() + "'s OK!    - unhandled exception");
        }

        try {
            wonderfulMethod();
        } catch (RuntimeInheritedException e) {
            System.out.println(e.getClass().getSimpleName() + " works too    - unhandled exception");
        }

        try {
            coolestMethod();
        } catch (ExceptionInheritedException e) {
            System.out.println(e.getClass().getSimpleName() + " also possible.    - handled exception");
        }
    }

    private static int amazingMethod() {
        if (true) throw new ErrorInheritedException();
        return 0;
    }

    private static int wonderfulMethod() {
        if (true) throw new RuntimeInheritedException();
        return 1;
    }

    private static int coolestMethod() throws ExceptionInheritedException {
        if (true) throw new ExceptionInheritedException();
        return 2;
    }
}
