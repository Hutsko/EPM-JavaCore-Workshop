package com.hutsko.codewars;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Jaden Smith, the son of Will Smith, is the star of films such as The Karate Kid (2010) and After Earth (2013). Jaden is also known for some of his philosophy that he delivers via Twitter. When writing on Twitter, he is known for almost always capitalizing every word. For simplicity, you'll have to capitalize each word, check out how contractions are expected to be in the example below.
 *
 * Your task is to convert strings to how they would be written by Jaden Smith. The strings are actual quotes from Jaden Smith, but they are not capitalized in the same way he originally typed them.
 *
 * Example:
 *
 * Not Jaden-Cased: "How can mirrors be real if our eyes aren't real"
 * Jaden-Cased:     "How Can Mirrors Be Real If Our Eyes Aren't Real"
 * Note that the Java version expects a return value of null for an empty string or null.
 */
class JadenCasingStrings {
    public static void main(String[] args) {
        System.out.println(toJadenCase("most trees are blue"));
        System.out.println(toJadenCaseByRegExp("most trees are blue"));
        System.out.println(toJadenCaseByLambda("most trees are blue"));
    }

    public static String toJadenCase(String phrase) {
        // TODO put your code below this comment
        if (phrase.isEmpty()) {
            return null;
        } else {
            char[] p = phrase.toCharArray();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < p.length; i++) {
                if (i == 0 || " ".equals(String.valueOf(p[i - 1]))) {
                    p[i] = Character.toUpperCase(p[i]);
                }
            }
            for (char c : p) {
                sb.append(c);
            }
            return sb.toString();
        }
    }

    public static String toJadenCaseByRegExp(String phrase) {
        if (phrase == null || phrase.isEmpty()) {
            return null;
        } else {
            StringBuilder sb = new StringBuilder();
            Pattern pattern = Pattern.compile("\\b[a-z]");
            Matcher m = pattern.matcher(phrase);
            int cursor = 0;
            while (m.find()) {
                sb.append(phrase, cursor, m.start());
                sb.append(m.group(0).toUpperCase());
                cursor = m.end();
            }
            sb.append(phrase.substring(cursor));
            return sb.toString();
        }
    }

    public static String toJadenCaseByLambda(String phrase) {
        if (phrase == null || phrase.isEmpty()) {
            return null;
        }
        return Stream.of(phrase.split(" "))
                .map(a -> a.substring(0,1).toUpperCase() + a.substring(1).toLowerCase())
                .collect(Collectors.joining(" "));
    }
}