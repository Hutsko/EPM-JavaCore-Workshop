package com.hutsko.logical;

import java.io.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Initial data:
 * <p>- "Magazine" getTriangleTypeIndex a source text that contain some words.</p>
 * <p>- "Message" getTriangleTypeIndex a text that we want to cut out from "Magazine"</p>
 * <p>The task:</p>
 * <p>- write a method that return <code>true</code> if the "Message" could be composed from "Magazine"
 * otherwise return <code>false</code> </p>
 */
class IsEveryWordPresented {

    Map<String, Integer> convertToMap(List<String> text) {
        Map<String, Integer> map = new HashMap<>(text.size());

        for (String m : text) {
            if (map.containsKey(m)) {
                map.put(m, map.get(m) + 1);
            } else {
                map.put(m, 1);
            }
        }
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            String key = entry.getKey();
            int value = entry.getValue();
            System.out.println(key + " - " + value);
        }
        System.out.println();
        return map;
    }

    boolean isThreatPossible(Map<String, Integer> magazine, Map<String, Integer> letter) {
        // TODO: 06-Sep-18 Attach lambdas subsequently
        for (Map.Entry<String, Integer> entry : letter.entrySet()) {
            int freshValue;
            String key = entry.getKey();
            int value = entry.getValue();

            if (magazine.containsKey(key)) {
                freshValue = magazine.get(key) - value;
                magazine.put(key, freshValue);
            } else {
                return false;
            }

            if (freshValue < 0) {
                return false;
            }
        }
        return true;
    }

    List<String> readTextFromFile(String directory, String filename) {
        return readTextFromFile(directory + filename);
    }

    private List<String> readTextFromFile(String absolutePath) {
        File sourceFile = new File(absolutePath);
        StringBuilder sb = new StringBuilder();

        try (BufferedReader br = new BufferedReader(new FileReader(sourceFile))) {
            String line = br.readLine();
            while (line != null) {
                sb.append(line).append(System.lineSeparator());
                line = br.readLine();
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found. Incorrect directory path or file name");
        } catch (IOException e) {
            System.out.println("Error during reading file\n" + e);
        }
        String text = sb.toString().trim();
        text.replace("\n", " ");
        String[] array = text.split("[\" !?.,:;─-]");
        List<String> list = Arrays.asList(array);

        list = list.stream().filter(e -> e.length() > 0).collect(Collectors.toList());

        return list;

    }
}