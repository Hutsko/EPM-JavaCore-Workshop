package com.hutsko.tdd.triangle;

public class TriangleAction {
    Triangle triangle;

    public TriangleAction(){}

    public int getTriangleTypeIndex() throws TriangleException {
        int sideA = triangle.getSideA();
        int sideB = triangle.getSideB();
        int sideC = triangle.getSideC();

        if (sideA == 0 || sideB == 0 || sideC == 0
                || (sideA > sideB + sideC || sideB > sideA + sideC || sideC > sideB + sideA)) {
            throw new TriangleException("A triangle can't be build. Wrong lengths of sides. " +
                    "The summ of two smaler sides must be higher than the third one");
        }

        if (sideA == sideB && sideA == sideC) {
            return 2;
        } else if (sideA == sideB || sideA == sideC || sideB == sideC) {
            return 1;
        } else {
            return 3;
        }
    }

    public void setTriangle(int a, int b, int c) {
        triangle = new Triangle(a, b, c);
    }
}
