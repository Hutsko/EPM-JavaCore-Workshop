package com.hutsko.tdd.triangle;

import java.util.Objects;

public class Triangle {

    private int sideA;
    private int sideB;
    private int sideC;

    public Triangle(int a, int b, int c){
        sideA = a;
        sideB = b;
        sideC = c;
    }

    public int getSideA(){
        return sideA;
    }

    public int getSideB() {
        return sideB;
    }

    public int getSideC() {
        return sideC;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Triangle)) return false;
        Triangle triangle = (Triangle) o;
        return sideA == triangle.sideA &&
                sideB == triangle.sideB &&
                sideC == triangle.sideC;
    }

    @Override
    public int hashCode() {
        return Objects.hash(sideA, sideB, sideC);
    }
}
